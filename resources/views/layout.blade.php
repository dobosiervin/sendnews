<!DOCTYPE html>
<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/adminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/adminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/adminLTE/bower_components/font-awesome/css/font-awesome.css") }}" rel="stylesheet" type="text/css" />
  {{--   <link rel="stylesheet" type="text/css" href="{{ asset("/adminLTE/plugins/datatables.min.css") }}"/> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset("/datatables.net-dt/css/jquery.dataTables.min.css") }}"/>
</head>
<body class="skin-blue sidebar-mini">
    <div class="wrapper">
    	<div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section><!-- /.content -->
        </div>
    </div>
	<!-- Scripts -->
     <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/adminLTE/bower_components/jquery/dist/jquery.min.js") }}"></script>
    <script src="{{ asset ("/adminLTE/bower_components/jquery/dist/jquery-ui.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/adminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
    @yield('js')
    <!-- AdminLTE App -->
    <script src="{{ asset ("/adminLTE/dist/js/adminlte.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/datatables.net/js/jquery.dataTables.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/datatables.net-dt/js/dataTables.dataTables.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/datatables.net-dt/js/dataTables.dataTables.min.js") }}" type="text/javascript"></script>
    {{-- <script src="{{ asset ("/adminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}" type="text/javascript"></script> --}}
    </body>
</html>
