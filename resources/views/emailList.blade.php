@extends('layout')

@section('content')
	<div id="table">
		<table id="data-table" class="display" width="100%">
			<thead class="bg-light-blue">
				<tr>
					<th>#</th>
					<th>Név</</th>
					<th>Email-cím</th>
					<th></th>

				</tr>
			</thead>
			<tfoot class="bg-light-blue">
                <tr>
                    <th>#</th>
					<th>Név</</th>
					<th>Email-cím</th>
					<th></th>
                </tr>
            </tfoot>
		</table>
	</div>
@endsection

@section('js')
    <script>
        var data = JSON.parse('<?php echo json_encode($data); ?>');
    </script>
	<script src="{{ URL::asset('/js/emaillist.js') }}" type="text/javascript"></script>
@endsection
