<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/master.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
</head>
<body>
	<div class="row">
		<div class="col-md-12">
				<div class="box-body">
					<form method="POST" id="pw_form">
						{{ csrf_field() }}
						<div class="form-group">
							<div class="input-group" title="Új jelszó" data-toggle="tooltip">
								<span class="input-group-addon iga-20-percent">Új jelszó</span>
								<input type="password" name="password" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group" title="Jelszó ismét" data-toggle="tooltip">
								<span class="input-group-addon iga-20-percent">Jelszó ismét</span>
								<input type="password" name="password_confirmation" class="form-control">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset ("/adminLTE/bower_components/jquery/dist/jquery.min.js") }}"></script>
    <script src="{{ asset ("/adminLTE/bower_components/jquery/dist/jquery-ui.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/adminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
</body>
</html>