<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//authentication
Auth::routes();
Route::get('/', function () {
    return redirect('/login');
});
Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@authenticate')->name('login');

//register
Route::get('/register', 'Auth\RegisterController@create');


Route::group(['middleware' => 'auth'], function () {
	Route::get('/admin', 'AdminController@mainPage');
});


Route::get('/cron', 'SendNewsController@newsQuery');
Route::get('/password/{password}', 'SendNewsController@showCashPassword');


// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/home', function () {
    return redirect('/admin');
});
