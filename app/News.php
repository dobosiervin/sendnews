<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id','news_id', 'title',
    ];

    public function newsemail_news()
    {
        return $this->hasMany('App\NewsEmail', 'news_id', 'id');
    }
}