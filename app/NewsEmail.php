<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsEmail extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'newsemail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'news_id', 'email_id',
    ];

    public function news_newsemail()
    {
    	return $this->belongsTo('App\News', 'id', 'news_id');
	}
	
	public function email_newsemail()
    {
    	return $this->belongsTo('App\User', 'id', 'email_id');
	}
}

