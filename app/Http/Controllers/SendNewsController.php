<?php

namespace App\Http\Controllers;

use App\User;
use App\News;
use App\NewsEmail;
use App\Mail\SendingNews;
use Illuminate\Http\Request;

class SendNewsController extends Controller
{
    public function newsQuery()
    {
    	//query fresh news
    	$url = 'https://onlineszamla.nav.gov.hu/api/news/visible/list?lang=hu';
    	$news = $this->curlPost($url);

    	//fresh news foreach
    	foreach ($news as $key => $newsData) {

    		//chack isset in DB fresh news
    		$freshNews = News::where('news_id', $newsData['id'])->first();
    		if (!isset($freshNews)) {
    			$news = News::create([
                	'news_id' => $newsData['id'], 
                	'title' => $newsData['title'],
            	]);
            }
			
			//chack send to all user fresh news
            $emailDB = User::all();
            foreach ($emailDB as $key => $user) {
            	$newsEmail = NewsEmail::where([
				    ['news_id', $freshNews['id'] ],
				    ['email_id', $user['id']],
				])->get();

            	//send news 
				if($newsEmail->count() < 1) {
					$this->sendMessage($newsData, $user['email']);

					//save sending
					$saveNewsMail = NewsEmail::create([
	                	'news_id' => $freshNews['id'],
					    'email_id' => $user['id']
					]);
				}
            }
		}
		echo "Megvagyok!";
		die();
    }

	public function curlPost($url)
	{
	    // create a new cURL resource
	    $ch = curl_init();
	    // set URL and other appropriate options
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, false);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 500); 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

	    $response = curl_exec($ch);
	    if (!$response) {
	        return null;
	    } else {
	        if ( strstr($response, '{') ) {
	            return json_decode($response, true);
	        }else{
	            return null;
	        }
	    }
	}

	public function sendMessage($news, $email)
	{
		$message = $news['body'];
		// Mail::to($email)->send(new SendingNews($message));
	}

	public function showCashPassword($password)
	{
		$cash_password = bcrypt($password);
		die(var_dump($cash_password));
	}
}
