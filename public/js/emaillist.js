var _targetIndex = -1;
function getTargetIndex() {
    return ++_targetIndex;
}
$(document).ready( function () {
$('#data-table').DataTable( {
			fixedHeader: {
	            header: true,
	            footer: true
	        },
		    data: data,
		    "columnDefs": [ {
			      "targets": getTargetIndex(),
			      "searchable": false,
			      'bSortalbe': true,
			      'data': 'id'
			    },
                {
			      "targets": getTargetIndex(),
			      "searchable": true,
			      'bSortalbe': true,
			      'data': 'user'
			    },
                {
			      "targets": getTargetIndex(),
			      "searchable": true,
			      'bSortalbe': true,
			      'data': 'email',
			    }, {
			      "targets": getTargetIndex(),
			      "searchable": false,
			      'bSortalbe': false,
			      'data': {
			      	'id':'id'
			      },
			      "render": function(data, type, full, meta) {
		               	return '<a href="jhkhj/'+data.id+'" class="btn btn-default">Módosítás</a>';
				    },
			    }, {
			      "targets": getTargetIndex(),
			      "searchable": false,
			      'bSortalbe': true,
			      'data':  {
			      	'id':'id'
			      },
			      "render": function(data, type, row, meta) {
	               	return	'<a href="hjhkjk/'+data.id+'" class="btn btn-default">Törlés</a>';
			    	},
		    	}
		    ]
		} );
} );
